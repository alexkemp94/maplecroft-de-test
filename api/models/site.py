from api.extensions import db


class Site(db.Model):
    """
    Basic Site model for CityBike locations fields including area admin code
    """

    id = db.Column(db.Integer, primary_key=True)

    company = db.Column(db.String(80), nullable=True)
    href = db.Column(db.String(80), nullable=True)
    cycle_id = db.Column(db.String(80), unique=True, nullable=True)
    city = db.Column(db.String(80), nullable=True)
    country = db.Column(db.String(80), nullable=True)
    latitude = db.Colum(db.Float, nullable=True)
    longitude = db.Colum(db.Float, nullable=True)
    name = db.Column(db.String(80), nullable=True)
    admin_area_code = db.Column(db.String(80), unique=True, nullable=True)
