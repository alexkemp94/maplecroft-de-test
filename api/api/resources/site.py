from flask_jwt_extended import jwt_required
from flask_restful import Resource
from api.models import Site


class SiteList(Resource):

    method_decorators = [jwt_required()]

    def get(self, admin_area):
        """
        Logic to filter records in the database for Site to return records based on ?admin_area API query.

        Message stating no records found if filter does not match any records, otherwise list of CityBike location attributes returned
        """

        # Query Site records to get all that match the input admin area code
        admin_area_sites = Site.query.filter_by(admin_area_code=admin_area).all()

        # Return message if no records found or return filtered records
        if admin_area_sites is None:
            return {"msg": "No sites found for admin area {}".format(admin_area)}
        else:
            return {"msg": admin_area_sites}
