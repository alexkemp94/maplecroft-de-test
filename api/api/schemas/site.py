"""
from api.models import Site
from api.extensions import ma, db


class SiteSchema(ma.SQLAlchemyAutoSchema):

    id = ma.Int(dump_only=True)

    class Meta:
        model = Site
        sqla_session = db.session
        load_instance = True
"""