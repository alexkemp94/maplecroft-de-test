import click
from flask.cli import with_appcontext


@click.group()
def cli():
    """Main entry point"""


@cli.command("init")
@with_appcontext
def init():
    """Create a new admin user"""
    from api.extensions import db
    from api.models import User

    click.echo("create user")
    user = User(username="admin", email="admin@mail.com", password="admin", active=True)
    db.session.add(user)
    db.session.commit()
    click.echo("created user admin")

@cli.command("load_sites")
@with_appcontext
def load_sites():
    """
    Obtains CityBike locations from API and obtain their admin area codes via GeoBoundaries. Steps include:

        - Get GeoBoundaries data from API
        - Extract all GeoBoundaries from json response, create shapely polygon object for each boundary and add to dictionary with admin_area_code
        - Add individual GeoBoundary dictionary to a list
        - Get CityBike data from API
        - Create shapely point object for each CiyBike location using lat, long values
        - Compare point with list of GeoBoundaries to check which boundary it is contained within
        - Once the correct boundary is found, obtain the admin area code
        - Format location attributes into a dictionary and add the dictionaries to a list
        - Clean the data in the new locations dictionary list
        - Insert each location in the locations dictionary list as a new record into the database

    NOTE: function can take ~15 minutes to run - majority of time spent is obtaining and converting the GeoBoundaries
    """

    import requests
    from shapely.geometry import Point
    from shapely.geometry.polygon import Polygon
    from api.extensions import db
    from api.models import Site

    click.echo("load sites")

    # Get GeoBoundaries data from API - use 'ALL' query parameters to ensure all admin area codes are captured
    gb_api_url = "https://www.geoboundaries.org/gbRequest.html?ISO=ALL&ADM=ALL"
    gb_data = requests.get(gb_api_url)

    # Get all geoboundary geometries and their admin area codes
    geoboundaries_list = []
    for result in gb_data.json():
        download_path = result["gjDownloadURL"]
        geoboundary = requests.get(download_path).json()

        for feat in geoboundary["features"]:
            admin_area_code = feat["properties"]["shapeID"]

            # Loop over sets of coordinates
            for coords in feat["geometry"]["coordinates"]:
                feature_dict = {}

                # Create polygon object using coordinates
                geom = coords[0]
                polygon = Polygon(geom)

                # Add admin_area_code and polygon to dictionary
                feature_dict["admin_area_code"] = admin_area_code
                feature_dict["polygon"] = polygon

                geoboundaries_list.append(feature_dict)

    # Get CityBikes data from API
    cb_api_url = "http://api.citybik.es/v2/networks"
    cb_data = requests.get(cb_api_url)

    # Link CityBike points to GeoBoundary polygons
    locations_dict_list = []
    for location in cb_data.json()["networks"]:
        location_dict = {}

        # Un-nest the location keys and add to new dictionary
        for key in location:
            if key == "location":
                for nested_key in location[key]:
                    location_dict[nested_key] = location[key][nested_key]

            else:
                location_dict[key] = location[key]

        # Create point object using lat long
        point = Point(location_dict["longitude"], location_dict["latitude"])

        # Get admin code
        for geoboundary in geoboundaries_list:
            polygon = geoboundary["polygon"]

            # Set admin code as empty string - in case it is not updated in the next few lines
            location_dict["admin_area_code"] = ""

            # Check whether point is in any of the geoboundaries - points in the sea (even slightly off the coast) will not be included
            if point.within(polygon):
                location_dict["admin_area_code"] = geoboundary["admin_area_code"]
                # Exit loop once admin area code has been obtained
                break

        locations_dict_list.append(location_dict)

    # Clean data before inserting it into the database
    for idx, record in enumerate(locations_dict_list):
        # Convert list to string before inserting into the database
        if type(record["company"]) is list:
            locations_dict_list[idx]["company"] = record["company"][0]

    # Insert data row by row into the database
    for record in locations_dict_list:
        # Instantiate site class with obtained attribute values and add row to database
        site = Site(
            company=record["company"],
            href=record["href"],
            cycle_id=record["id"],
            city=record["city"],
            country=record["country"],
            latitude=record["latitude"],
            longitude=record["longitude"],
            name=record["name"],
            admin_area_code=record["admin_area_code"]
        )
        db.session.add(site)
    db.session.commit()

    click.echo("loaded sites")
    return


if __name__ == "__main__":
    cli()
